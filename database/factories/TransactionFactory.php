<?php

namespace Database\Factories;

use App\Models\PaymentCard;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->randomNumber(),
            'description' => $this->faker->text(150),
            'type' => $this->faker->randomElement(['paya','internal']),
            'status' => $this->faker->randomElement(['done','failed']),
            'error' => $this->faker->text(100),
            'ref_code' => $this->faker->numberBetween(1111111111, 9999999999),
            'track_id' => $this->faker->numberBetween(1111111111, 9999999999),
            'reason_description' => $this->faker->text(120),
            'payment_number' => $this->faker->randomDigit(),
            'inquiry_date' => $this->faker->creditCardExpirationDateString(),
            'inquiry_sequence' => $this->faker->randomDigit(),
            'inquiry_time' => $this->faker->date('H:i'),
            'source_payment_card_id' => PaymentCard::factory()->create()->id,
            'destination_payment_card_id' => PaymentCard::factory()->create()->id,
            'user_id' => User::factory()->create()->id,
        ];
    }
}
