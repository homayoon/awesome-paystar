<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstname' => $this->faker->firstName(),
            'lastname' => $this->faker->lastName(),
            'card_number' => $this->faker->creditCardNumber(),
            'card_expire_date' => $this->faker->creditCardExpirationDateString(),
            'user_id' => User::factory()->create()->id
        ];
    }
}
