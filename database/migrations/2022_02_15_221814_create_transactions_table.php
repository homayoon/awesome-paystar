<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount',15,2);
            $table->text('description')->nullable();
            $table->string('type')->default('internal');
            $table->string('status');
            $table->text('error');
            $table->string('ref_code')->nullable();
            $table->string('track_id');
            $table->text('reason_description')->nullable();
            $table->string('payment_number')->nullable();
            $table->string('inquiry_date')->nullable();
            $table->string('inquiry_sequence')->nullable();
            $table->string('inquiry_time')->nullable();
            $table->bigInteger('source_payment_card_id')->unsigned();
            $table->bigInteger('destination_payment_card_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('source_payment_card_id')
                ->references('id')
                ->on('payment_cards')
                ->onUpdate('cascade');

            $table->foreign('destination_payment_card_id')
                ->references('id')
                ->on('payment_cards')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
