<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $actor;

    public function getActor() :Model
    {
        return $this->actor;
    }

    public function setActor(Model $user): void
    {
        $this->actor = $user;
        $this->actingAs($this->actor);
    }
}
