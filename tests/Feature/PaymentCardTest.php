<?php

namespace Tests\Feature;

use App\Models\PaymentCard;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentCardTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setActor(User::factory()->create());
    }

    /** @test */
    public function user_can_get_payment_cards_list()
    {
        PaymentCard::factory()->count(50)->create();
        $this->getJson('/api/v1/payment-card')
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'current_page' => 1
                ]
            ])->assertJsonStructure($this->paymentCardCollectionStructure());
    }

    /** @test */
    public function payment_card_can_be_created()
    {
        $payment_card = PaymentCard::factory()->make()->toArray();
        $this->postJson('/api/v1/payment-card',$payment_card)
            ->assertStatus(201)
            ->assertJsonStructure($this->paymentCardResourceStructure());
    }

    /** @test */
    public function payment_card_cannot_be_create_with_invalid_inputs()
    {
        $this->postJson('/api/v1/payment-card')
            ->assertStatus(422)
            ->assertJsonValidationErrors($this->validationInputs());
    }

    /** @test */
    public function can_update_payment_card()
    {
        $payment_card = PaymentCard::factory()->create();
        $newValues = PaymentCard::factory()->make()->toArray();
        $this->patchJson("/api/v1/payment-card/{$payment_card->id}",$newValues)
            ->assertStatus(200)
            ->assertJsonFragment($this->paymentCardResourceFragment($newValues))
            ->assertJsonStructure($this->paymentCardResourceStructure());
    }

    /** @test */
    public function payment_card_cannot_be_updated_with_invalid_inputs()
    {
        $payment_card = PaymentCard::factory()->create();
        $this->patchJson("/api/v1/payment-card/{$payment_card->id}")
            ->assertStatus(422)
            ->assertJsonValidationErrors($this->validationInputs());
    }

    /** @test */
    public function can_delete_payment_card()
    {
        $payment_card = PaymentCard::factory()->create();
        $this->deleteJson("/api/v1/payment-card/{$payment_card->id}")
            ->assertStatus(200)
            ->assertJsonStructure($this->paymentCardResourceStructure());
        $this->assertDatabaseMissing('payment_cards',[
            'id' => $payment_card->id
        ]);
    }

    private function validationInputs() {
        return ['card_number', 'firstname', 'lastname', 'card_expire_date'];
    }

    private function paymentCardResourceStructure()
    {
        return [
            'message',
            'payment_card' => [
                'id', 'firstname', 'lastname','card_expire_date', 'card_number',
                'payed_transactions' ,'received_transactions',
                'created_at', 'updated_at'
            ]
        ];
    }

    private function paymentCardCollectionStructure()
    {
        return [
            'message',
            'payment_cards',
            'meta'
        ];
    }

    private function paymentCardResourceFragment(array $paymentCard){
        return [
            'firstname' => $paymentCard['firstname'],
            'lastname' => $paymentCard['lastname'],
            'card_expire_date' => $paymentCard['card_expire_date'],
            'card_number' => $paymentCard['card_number']
        ];
    }
}
