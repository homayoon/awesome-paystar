<?php

namespace Tests\Feature;

use App\Models\PaymentCard;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setActor(User::factory()->create());
    }

    /** @test */
    public function user_can_get_transactions_list()
    {
        Transaction::factory()->count(50)->create();
        $this->getJson('/api/v1/transaction')
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'current_page' => 1
                ]
            ])->assertJsonStructure($this->transactionCollectionStructure());
    }

    /** @test */
    public function user_can_transform_deposit()
    {
        $this->postJson('/api/v1/transaction', $this->fakeTransactionRequest())
            ->assertStatus(201)
            ->assertJsonStructure($this->transactionResourceStructure());
    }

    /** @test */
    public function user_cannot_transform_deposit_with_invalid_inputs()
    {
        $this->postJson('/api/v1/transaction')
            ->assertStatus(422)
            ->assertJsonValidationErrors($this->validationInputs());
    }

    private function validationInputs() {
        return ['source_payment_card_id', 'destination_payment_card_id', 'description', 'amount'];
    }

    private function transactionResourceStructure()
    {
        return [
            'message',
            'transaction' => [
                'id', 'source_payment_card','destination_payment_card',
                'user', 'description', 'amount', 'type', 'status',
                'error', 'ref_code', 'track_id', 'reason_description',
                'payment_number', 'inquiry_date', 'inquiry_sequence',
                'inquiry_time', 'created_at', 'updated_at'
            ]
        ];
    }

    private function transactionCollectionStructure()
    {
        return [
            'message',
            'transactions',
            'meta'
        ];
    }

    private function transactionResourceFragment(array $transaction){
        return [
            'amount' => $transaction['amount'],
            'description' => $transaction['description'],
            'type' => $transaction['type'],
            'status' => $transaction['status'],
            'error' => $transaction['error'],
            'ref_code' => $transaction['ref_code'],
            'track_id' => $transaction['track_id'],
            'reason_description' => $transaction['reason_description'],
            'payment_number' => $transaction['payment_number'],
            'inquiry_date' => $transaction['inquiry_date'],
            'inquiry_sequence' => $transaction['inquiry_sequence'],
            'inquiry_time' => $transaction['inquiry_time'],
        ];
    }

    private function fakeTransactionRequest()
    {
        return [
            'amount' => $this->faker->randomNumber(),
            'description' => $this->faker->text(150),
            'source_payment_card_id' => PaymentCard::factory()->create()->id,
            'destination_payment_card_id' => PaymentCard::factory()->create()->id,
        ];
    }
}
