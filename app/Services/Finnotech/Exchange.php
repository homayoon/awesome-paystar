<?php


namespace App\Services\Finnotech;


use App\Models\PaymentCard;
use App\Models\Transaction;
use App\Services\Bank\BankService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;

class Exchange implements BankService
{
    protected $token;
    protected $clientID;

    public function __construct()
    {
        $this->token = env('services.finnotech.token');
        $this->clientID = env('services.finnotech.client_id');
    }

    public function makeRequest($scope, $method, $data)
    {
        $address = "https://apibeta.finnotech.ir/oak/v2/clients/{$this->clientID}/$scope";
        return Http::withToken($this->token)->{$method}($address, $data);
    }

    public function exchange(Transaction $transaction)
    {
        return $this->makeRequest('transferTo','post',$transaction->toArray());
    }

    public function fakeTransactionData($data)
    {
        return Transaction::factory()->state([
            'amount' => $data['amount'],
            'description' => $data['description'],
            'source_payment_card_id' => $data['source_payment_card_id'],
            'destination_payment_card_id' => $data['destination_payment_card_id'],
        ])->make()->toArray();
    }
}
