<?php


namespace App\Services\Bank;


use App\Models\PaymentCard;
use App\Models\Transaction;

interface BankService
{
    public function makeRequest($scope, $method, $data);

    public function exchange(Transaction $transaction);

    public function fakeTransactionData(array $data);
}
