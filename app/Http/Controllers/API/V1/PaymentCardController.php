<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentCardRequest;
use App\Http\Resources\V1\PaymentCard\PaymentCardCollection;
use App\Http\Resources\V1\PaymentCard\PaymentCardResource;
use App\Models\PaymentCard;

class PaymentCardController extends Controller
{
    private $fillableInputs = ['firstname', 'lastname', 'card_number', 'card_expire_date'];

    public function index(PaymentCardRequest $request)
    {
        $perPage = isset($request->perPage) && $request->perPage ? $request->perPage : 20;
        $cards = PaymentCard::latest()->paginate($perPage);
        return new PaymentCardCollection($cards);
    }

    public function store(PaymentCardRequest $request)
    {
        $paymentCard = auth()->user()->paymentCards()->create($request->only($this->fillableInputs));
        return new PaymentCardResource($paymentCard);
    }

    public function update(PaymentCardRequest $request, PaymentCard $paymentCard)
    {
        $paymentCard->update($request->only($this->fillableInputs));
        return new PaymentCardResource($paymentCard);
    }

    public function destroy(PaymentCard $paymentCard)
    {
        $paymentCard->delete();
        return new PaymentCardResource($paymentCard);
    }
}
