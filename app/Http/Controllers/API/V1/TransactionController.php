<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionRequest;
use App\Http\Resources\V1\Transaction\TransactionCollection;
use App\Http\Resources\V1\Transaction\TransactionResource;
use App\Models\Transaction;
use App\Services\Bank\BankService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $bankService;
    private $fillableInputs = ['description', 'amount', 'source_payment_card_id', 'destination_payment_card_id'];

    public function __construct(BankService $service)
    {
        $this->bankService = $service;
    }

    public function index(TransactionRequest $request)
    {
        $perPage = isset($request->perPage) && $request->perPage ? $request->perPage : 20;
        $transactions = Transaction::latest()->paginate($perPage);
        return new TransactionCollection($transactions);
    }

    public function deposit(TransactionRequest $request)
    {
        $transactionData = $this->bankService->fakeTransactionData($request->only($this->fillableInputs));
        $transaction = auth()->user()->transactions()->create($transactionData);
        try {
            $this->bankService->exchange($transaction);
        }catch (\Exception $exception){
            //
        }
        return new TransactionResource($transaction);
    }
}
