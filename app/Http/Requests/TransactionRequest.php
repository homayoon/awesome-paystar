<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class TransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this->route()->getName();
        switch ($routeName){
            case Str::contains($routeName,['transaction.deposit']):
                return [
                    'amount' => ['required'],
                    'description' => ['required'],
                    'source_payment_card_id' => ['required'],
                    'destination_payment_card_id' => ['required']
                ];
            default:
                return [];
        }
    }
}
