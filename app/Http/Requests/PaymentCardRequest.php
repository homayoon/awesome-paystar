<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class PaymentCardRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this->route()->getName();
        switch ($routeName){
            case Str::contains($routeName,['payment-card.update', 'payment-card.store']):
                return [
                    'firstname' => ['required'],
                    'lastname' => ['required'],
                    'card_number' => ['required'],
                    'card_expire_date' => ['required']
                ];
            default:
                return [];
        }
    }
}
