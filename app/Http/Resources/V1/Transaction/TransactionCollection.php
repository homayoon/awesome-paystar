<?php

namespace App\Http\Resources\V1\Transaction;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TransactionCollection extends ResourceCollection
{

    public static $wrap = 'transactions';

    public function toArray($request)
    {
        return $this->collection->map(function ($transaction) {
            return [
                'id' => $transaction->id,
                'amount' => $transaction->amount,
                'description' => $transaction->description,
                'type' => $transaction->type,
                'status' => $transaction->status,
                'error' => $transaction->error,
                'ref_code' => $transaction->ref_code,
                'track_id' => $transaction->track_id,
                'reason_description' => $transaction->reason_description,
                'payment_number' => $transaction->payment_number,
                'inquiry_date' => $transaction->inquiry_date,
                'inquiry_sequence' => $transaction->inquiry_sequence,
                'inquiry_time' => $transaction->inquiry_time,
                'source_payment_card' => $transaction->sourceCard,
                'destination_payment_card' => $transaction->destinationCard,
                'user' => $transaction->user,
                'created_at' => $transaction->created_at,
                'updated_at' => $transaction->updated_at
            ];
        });
    }

    public function with($request)
    {
        return [
            'message' => ''
        ];
    }
}
