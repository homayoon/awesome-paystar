<?php

namespace App\Http\Resources\V1\Transaction;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{

    public static $wrap = 'transaction';

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'description' => $this->description,
            'type' => $this->type,
            'status' => $this->status,
            'error' => $this->error,
            'ref_code' => $this->ref_code,
            'track_id' => $this->track_id,
            'reason_description' => $this->reason_description,
            'payment_number' => $this->payment_number,
            'inquiry_date' => $this->inquiry_date,
            'inquiry_sequence' => $this->inquiry_sequence,
            'inquiry_time' => $this->inquiry_time,
            'source_payment_card' => $this->sourceCard,
            'destination_payment_card' => $this->destinationCard,
            'user' => $this->user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }

    public function with($request)
    {
        return [
            'message' => ''
        ];
    }
}
