<?php

namespace App\Http\Resources\V1\PaymentCard;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PaymentCardCollection extends ResourceCollection
{

    public static $wrap = 'payment_cards';

    public function toArray($request)
    {
        return $this->collection->map(function ($card) {
            return [
                'id' => $card->id,
                'firstname' => $card->firstname,
                'lastname' => $card->lastname,
                'card_number' => $card->card_number,
                'card_expire_date' => $card->card_expire_date,
                'user' => $card->user,
                'payed_transactions' => $card->payedTransactions()->latest()->get(),
                'received_transactions' => $card->receivedTransactions()->latest()->get(),
                'created_at' => $card->created_at,
                'updated_at' => $card->updated_at
            ];
        });
    }

    public function with($request)
    {
        return [
            'message' => ''
        ];
    }
}
