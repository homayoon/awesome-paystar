<?php

namespace App\Http\Resources\V1\PaymentCard;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentCardResource extends JsonResource
{

    public static $wrap = 'payment_card';

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'card_number' => $this->card_number,
            'card_expire_date' => $this->card_expire_date,
            'user' => $this->user,
            'payed_transactions' => $this->payedTransactions()->latest()->get(),
            'received_transactions' => $this->receivedTransactions()->latest()->get(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }

    public function with($request)
    {
        return [
            'message' => ''
        ];
    }
}
