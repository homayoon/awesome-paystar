<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount', 'description', 'type', 'status',
        'error', 'ref_code', 'track_id', 'reason_description',
        'payment_number', 'inquiry_date', 'inquiry_sequence',
        'inquiry_time', 'source_payment_card_id',
        'destination_payment_card_id', 'user_id'
    ];

    /*
     * Relations
     */

    public function sourceCard() :BelongsTo
    {
        return $this->belongsTo(PaymentCard::class,'source_payment_card_id');
    }

    public function destinationCard() :BelongsTo
    {
        return $this->belongsTo(PaymentCard::class,'destination_payment_card_id');
    }

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
