<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PaymentCard extends Model
{
    use HasFactory;

    protected $fillable = [
        'firstname', 'lastname', 'card_number',
        'card_expire_date', 'user_id'
    ];

    /*
     * Relations
     */

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function payedTransactions() :HasMany
    {
        return $this->hasMany(Transaction::class,'source_payment_card_id');
    }

    public function receivedTransactions() :HasMany
    {
        return $this->hasMany(Transaction::class,'destination_payment_card_id');
    }

}
