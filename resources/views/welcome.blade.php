<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Awesome Paystar</title>

    <link href="{{asset('assets/css/app.css')}}" rel="stylesheet">
</head>
<body>
{{--  Application Container  --}}
<div id="applicationContainer"></div>

<script src="{{asset('assets/js/app.js')}}"></script>
</body>
</html>
